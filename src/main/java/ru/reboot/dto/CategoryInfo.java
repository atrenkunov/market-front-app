package ru.reboot.dto;

public class CategoryInfo {

    private String id;
    private String name;

    public CategoryInfo(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
