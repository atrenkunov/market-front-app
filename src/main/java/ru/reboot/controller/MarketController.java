package ru.reboot.controller;

import ru.reboot.dto.CategoryInfo;
import ru.reboot.dto.ItemInfo;

import java.util.List;

public interface MarketController {

    /**
     * Get available categories.
     * Returns empty collection if no category found.
     */
    List<CategoryInfo> getCategories();

    /**
     * Get all items by category with assigned price.
     */
    List<ItemInfo> getItemsByCategory(String categoryId);
}
