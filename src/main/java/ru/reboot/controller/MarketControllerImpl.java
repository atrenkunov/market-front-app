package ru.reboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.reboot.dto.CategoryInfo;
import ru.reboot.dto.ItemInfo;
import ru.reboot.service.MarketService;

import java.util.Date;
import java.util.List;

/**
 * Price controller.
 */
@RestController
@RequestMapping(path = "market")
public class MarketControllerImpl implements MarketController {

    private static final Logger logger = LogManager.getLogger(MarketControllerImpl.class);

    private MarketService marketService;

    @Autowired
    public void setMarketService(MarketService marketService) {
        this.marketService = marketService;
    }

    @GetMapping("info")
    public String info() {
        logger.info("method .info invoked");
        return "MarketController " + new Date();
    }

    @Override
    public List<CategoryInfo> getCategories() {
        return marketService.getCategories();
    }

    @Override
    public List<ItemInfo> getItemsByCategory(String category) {
        return marketService.getItemsByCategory(category);
    }
}
