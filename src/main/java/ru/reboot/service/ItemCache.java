package ru.reboot.service;

import org.springframework.stereotype.Component;
import ru.reboot.dto.ItemInfo;

/**
 * Item cache. Loads on startup
 */
@Component
public class ItemCache {

    /**
     * Get item by item id.
     *
     * @param itemId - item id
     * @return item or null if item doesn't exists
     */
    public ItemInfo get(String itemId) {
        return null;
    }
}
