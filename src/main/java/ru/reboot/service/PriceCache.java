package ru.reboot.service;

import org.springframework.stereotype.Component;

/**
 * Item cache. Loads on startup.
 */
@Component
public class PriceCache {

    /**
     * Get item by item id.
     *
     * @param itemId - item id
     * @return item or null if item doesn't exists
     */
    public Double get(String itemId) {
        return null;
    }

    /**
     * Check if item has assigned price.
     *
     * @param itemId - item id
     */
    public boolean contains(String itemId) {
        return false;
    }
}
