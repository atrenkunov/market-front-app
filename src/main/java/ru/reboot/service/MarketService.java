
package ru.reboot.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.reboot.dto.CategoryInfo;
import ru.reboot.dto.ItemInfo;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class MarketService {

    private static final Logger logger = LogManager.getLogger(MarketService.class);

    private ItemCache itemCache;
    private PriceCache priceCache;

    @Autowired
    public void setItemCache(ItemCache itemCache) {
        this.itemCache = itemCache;
    }

    @Autowired
    public void setPriceCache(PriceCache priceCache) {
        this.priceCache = priceCache;
    }

    /**
     * Get available categories.
     * Returns empty collection if no category found.
     */
    public List<CategoryInfo> getCategories() {
        return null;
    }

    /**
     * Get all items by category with assigned price.
     */
    public List<ItemInfo> getItemsByCategory(String category) {
        return null;
    }


    /**
     * Init cache here.
     */
    @PostConstruct
    public void init() {

        initializePriceCache();
        initializeItemCache();
    }

    private void initializePriceCache() {
    }

    private void initializeItemCache() {
    }
}
